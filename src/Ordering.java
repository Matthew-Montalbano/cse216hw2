import java.util.*;

public class Ordering {

    static class XLocationComparator implements Comparator<TwoDShape> {
        @Override public int compare(TwoDShape o1, TwoDShape o2) {
            double x1, x2;

            if (o1 instanceof Circle) {
                x1 = getCenterMinusRadius(o1);
            } else {
                List<TwoDPoint> points = getQuadrilateralVertices(o1);
                x1 = getSmallestXValue(points);
            }

            if (o2 instanceof Circle) {
                x2 = getCenterMinusRadius(o2);
            } else {
                List<TwoDPoint> points = getQuadrilateralVertices(o2);
                x2 = getSmallestXValue(points);
            }

            if (x1 > x2) {
                return 1;
            } else if (x1 < x2) {
                return -1;
            } else {
                return 0;
            }
        }

        private double getCenterMinusRadius(TwoDShape shape) {
            Circle circle = (Circle) shape;
            return circle.center().coordinates()[0] - circle.getRadius();
        }

        private List<TwoDPoint> getQuadrilateralVertices(TwoDShape shape) {
            Quadrilateral quad = (Quadrilateral) shape;
            return quad.getPosition();
        }

        private double getSmallestXValue(List<TwoDPoint> points) {
            List<Double> xValues = new ArrayList<>();
            for (TwoDPoint point: points) {
                xValues.add(point.coordinates()[0]);
            }
            return Collections.min(xValues);
        }
    }

    static class AreaComparator implements Comparator<SymmetricTwoDShape> {
        @Override public int compare(SymmetricTwoDShape o1, SymmetricTwoDShape o2) {
            if (o1.area() > o2.area()) {
                return 1;
            } else if (o1.area() < o2.area()) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    static class SurfaceAreaComparator implements Comparator<ThreeDShape> {
        @Override public int compare(ThreeDShape o1, ThreeDShape o2) {
            double surfaceArea1, surfaceArea2;

            if (o1 instanceof Cuboid) {
                surfaceArea1 = surfaceAreaCuboid(o1);
            } else {
                surfaceArea1 = surfaceAreaSphere(o1);
            }

            if (o2 instanceof Cuboid) {
                surfaceArea2 = surfaceAreaCuboid(o2);
            } else {
                surfaceArea2 = surfaceAreaSphere(o2);
            }

            if (surfaceArea1 > surfaceArea2) {
                return 1;
            } else if (surfaceArea1 < surfaceArea2) {
                return -1;
            } else {
                return 0;
            }
        }

        private double surfaceAreaCuboid(ThreeDShape shape) {
            Cuboid cuboid = (Cuboid) shape;
            return cuboid.surfaceArea();
        }

        private double surfaceAreaSphere(ThreeDShape shape) {
            Sphere sphere = (Sphere) shape;
            return sphere.surfaceArea();
        }
    }

    // TODO: there's a lot wrong with this method. correct it so that it can work properly with generics.
    /*
    static void copy(List<TwoDShape> source, List<TwoDShape> destination) {
        destination.addAll(source);
    }*/

    static <T> void copy(Collection<? extends T> source, Collection<T> destination) {
        destination.addAll(source);
    }

    public static void main(String[] args) {
        List<TwoDShape>          shapes          = new ArrayList<>();
        List<SymmetricTwoDShape> symmetricshapes = new ArrayList<>();
        List<ThreeDShape>        threedshapes    = new ArrayList<>();

        /*
         * uncomment the following block and fill in the "..." constructors to create actual instances. If your
         * implementations are correct, then the code should compile and yield the expected results of the various
         * shapes being ordered by their smallest x-coordinate, area, volume, surface area, etc. */


        symmetricshapes.add(new Rectangle(-50d, 5d, -50d, -10d, 10d, -10d, 10d, 5d));
        symmetricshapes.add(new Square(-5.1, 5.1, -5.1, 0, 0, 0, 0, 5.1));
        symmetricshapes.add(new Circle(10, 10, 5));

        copy(symmetricshapes, shapes); // note-1 //
        shapes.add(new Quadrilateral(-5.1, 5.1, -5.1, -1, 0, 0, 0, 5.1));


        // sorting 2d shapes according to various criteria
        shapes.sort(new XLocationComparator());
        symmetricshapes.sort(new XLocationComparator());
        symmetricshapes.sort(new AreaComparator());

        // sorting 3d shapes according to various criteria
        Collections.sort(threedshapes);
        threedshapes.sort(new SurfaceAreaComparator());

        /*
         * if your changes to copy() are correct, uncommenting the following block will also work as expected note that
         * copy() should work for the line commented with 'note-1' while at the same time also working with the lines
         * commented with 'note-2' and 'note-3'. */


        List<Number> numbers = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        Set<Square>        squares = new HashSet<>();
        Set<Quadrilateral> quads   = new LinkedHashSet<>();

        copy(doubles, numbers); // note-2 //
        copy(squares, quads);   // note-3 //

    }
}
