public class Sphere implements ThreeDShape {

    private ThreeDPoint center;
    private double radius;

    public Sphere(double centerX, double centerY, double centerZ, double radius) {
        this.center = new ThreeDPoint(centerX, centerY, centerZ);
        this.radius = radius;
    }

    @Override
    public ThreeDPoint center() {
        return center;
    }

    @Override
    public double volume() {
        return (4.0 / 3.0) * Math.PI * Math.pow(this.radius, 3);
    }

    public double surfaceArea() {
        return (4.0) * Math.PI * Math.pow(this.radius, 2);
    }

    public static Sphere random() {
        double x = getRandomValue();
        double y = getRandomValue();
        double z = getRandomValue();
        double radius = Math.random() * 100;
        return new Sphere(x, y, z, radius);
    }

    private static double getRandomValue() {
        if (Math.random() >= 0.5) {
            return -1 * Math.random() * 100;
        } else {
            return Math.random() * 100;
        }
    }

    @Override
    public int compareTo(ThreeDShape otherShape) {
        if (volume() > otherShape.volume()) {
            return 1;
        } else if (volume() < otherShape.volume()) {
            return -1;
        } else {
            return 0;
        }
    }
}
