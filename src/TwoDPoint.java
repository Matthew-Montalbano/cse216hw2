import java.util.List;
import java.util.ArrayList;

/**
 * An unmodifiable point in the standard two-dimensional Euclidean space. The coordinates of such a point is given by
 * exactly two doubles specifying its <code>x</code> and <code>y</code> values.
 */
public class TwoDPoint implements Point {
    private double[] coordinates;

    public TwoDPoint(double x, double y) {
        coordinates = new double[2];
        coordinates[0] = x;
        coordinates[1] = y;
    }

    /**
     * @return the coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
        return coordinates;
    }

    /**
     * Returns a list of <code>TwoDPoint</code>s based on the specified array of doubles. A valid argument must always
     * be an even number of doubles so that every pair can be used to form a single <code>TwoDPoint</code> to be added
     * to the returned list of points.
     *
     * @param coordinates the specified array of doubles.
     * @return a list of two-dimensional point objects.
     * @throws IllegalArgumentException if the input array has an odd number of doubles.
     */
    public static List<TwoDPoint> ofDoubles(double... coordinates) throws IllegalArgumentException {
        if (coordinates.length % 2 == 1) {
            throw new IllegalArgumentException("Input array has an odd number of doubles.");
        }
        List<TwoDPoint> pointsList = new ArrayList<TwoDPoint>();
        int index = 0;
        while (index < coordinates.length) {
            pointsList.add(new TwoDPoint(coordinates[index], coordinates[index+1]));
            index += 2;
        }
        return pointsList;
    }
}
