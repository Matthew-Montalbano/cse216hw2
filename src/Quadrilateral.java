import java.util.Arrays;
import java.util.List;

public class Quadrilateral implements Positionable, TwoDShape {

    private final TwoDPoint[] vertices = new TwoDPoint[4];

    public Quadrilateral(double... vertices) { 
        this(TwoDPoint.ofDoubles(vertices));
    }

    public Quadrilateral(List<TwoDPoint> vertices) {
        int n = 0;
        for (TwoDPoint p : vertices) this.vertices[n++] = p;
        if (!isMember(vertices))
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getCanonicalName()));
    }

    /**
     * Given a list of four points, adds them as the four vertices of this quadrilateral in the order provided in the
     * list. This is expected to be a counterclockwise order of the four corners.
     *
     * @param points the specified list of points.
     * @throws IllegalStateException if the number of vertices provided as input is not equal to four.
     */
    @Override
    public void setPosition(List<? extends Point> points) {
        if (!isMember(points)) throw new IllegalStateException("Number of vertices not equal to four.");
        for (int i = 0; i < points.size(); i++) {
            Point currentPoint = points.get(i);
            vertices[i] = new TwoDPoint(currentPoint.coordinates()[0], currentPoint.coordinates()[1]);
        }
    }

    @Override
    public List<TwoDPoint> getPosition() {
        return Arrays.asList(vertices);
    }

    /**
     * @return the lengths of the four sides of the quadrilateral. Since the setter {@link Quadrilateral#setPosition(List)}
     *         expected the corners to be provided in a counterclockwise order, the side lengths are expected to be in
     *         that same order.
     */
    protected double[] getSideLengths() {
        double[] sideLengths = new double[4];
        for (int i = 0; i < vertices.length - 1; i++) {
            sideLengths[i] = calculateLength(vertices[i], vertices[i+1]);
        }
        sideLengths[3] = calculateLength(vertices[3], vertices[0]);
        return sideLengths;
    }

    protected double calculateLength(TwoDPoint pointOne, TwoDPoint pointTwo) {
        double xDistance = Math.pow(pointTwo.coordinates()[0] - pointOne.coordinates()[0], 2);
        double yDistance = Math.pow(pointTwo.coordinates()[1] - pointOne.coordinates()[1], 2);
        return Math.sqrt(xDistance + yDistance);
    }

    @Override
    public int numSides() { return 4; }

    @Override
    public boolean isMember(List<? extends Point> vertices) { return vertices.size() == 4; }
}
