import java.util.List;
import java.util.Arrays;

// TODO : a missing interface method must be implemented in this class to make it compile. This must be in terms of volume().
public class Cuboid implements ThreeDShape {

    private final ThreeDPoint[] vertices = new ThreeDPoint[8];

    /**
     * Creates a cuboid out of the list of vertices. It is expected that the vertices are provided in
     * the order as shown in the figure given in the homework document (from v0 to v7).
     * 
     * @param vertices the specified list of vertices in three-dimensional space.
     */
    public Cuboid(List<ThreeDPoint> vertices) {
        if (vertices.size() != 8)
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getName()));
        int n = 0;
        for (ThreeDPoint p : vertices) this.vertices[n++] = p;
    }

    @Override
    public double volume() {
        double length, width, height;
        length = calculateDistance(vertices[0], vertices[1]);
        height = calculateDistance(vertices[1], vertices[2]);
        width = calculateDistance(vertices[1], vertices[6]);
        return (length * width * height);
    }

    @Override
    public ThreeDPoint center() {
        double xCenter, yCenter, zCenter;
        xCenter = vertices[0].coordinates()[0] - ((vertices[0].coordinates()[0] - vertices[1].coordinates()[0]) / 2);
        yCenter = vertices[1].coordinates()[1] - ((vertices[1].coordinates()[1] - vertices[2].coordinates()[1]) / 2);
        zCenter = vertices[4].coordinates()[2] - ((vertices[4].coordinates()[2] - vertices[3].coordinates()[2]) / 2);
        return new ThreeDPoint(xCenter, yCenter, zCenter);
    }

    public double surfaceArea() {
        double length, width, height;
        length = calculateDistance(vertices[0], vertices[1]);
        height = calculateDistance(vertices[1], vertices[2]);
        width = calculateDistance(vertices[1], vertices[6]);
        return (2 * (length * width) + (width * height) + (height * length));
    }

    private double calculateDistance(ThreeDPoint pointOne, ThreeDPoint pointTwo) {
        double xDistance = Math.pow(pointTwo.coordinates()[0] - pointOne.coordinates()[0], 2);
        double yDistance = Math.pow(pointTwo.coordinates()[1] - pointOne.coordinates()[1], 2);
        double zDistance = Math.pow(pointTwo.coordinates()[2] - pointOne.coordinates()[2], 2);
        return Math.sqrt(xDistance + yDistance + zDistance);
    }

    public static Cuboid random() {
        double x = getRandomValue();
        double y = getRandomValue();
        double z = getRandomValue();
        double length = Math.random() * 100;
        double width = Math.random() * 100;
        double height = Math.random() * 100;
        ThreeDPoint[] vertices = new ThreeDPoint[8];
        vertices[0] = new ThreeDPoint(x, y, z);
        vertices[1] = new ThreeDPoint(x - length, y, z);
        vertices[3] = new ThreeDPoint(x, y - height, z);
        vertices[5] = new ThreeDPoint(x, y, z + width);
        vertices[2] = new ThreeDPoint(x - length, y - height, z);
        vertices[6] = new ThreeDPoint(x - length, y, z + width);
        vertices[7] = new ThreeDPoint(x - length, y - height, z + width);
        vertices[4] = new ThreeDPoint(x, y - height, z + width);
        return new Cuboid(Arrays.asList(vertices));
    }

    private static double getRandomValue() {
        if (Math.random() >= 0.5) {
            return -1 * Math.random() * 100;
        } else {
            return Math.random() * 100;
        }
    }

    @Override
    public int compareTo(ThreeDShape otherShape) {
        if (volume() > otherShape.volume()) {
            return 1;
        } else if (volume() < otherShape.volume()) {
            return -1;
        } else {
            return 0;
        }
    }
}
