import java.util.List;

public class Rectangle extends Quadrilateral implements SymmetricTwoDShape {

    public Rectangle(double... vertices) {
        super(vertices);
    }

    public Rectangle(List<TwoDPoint> vertices) {
        super(vertices);
    }

    /**
     * The center of a rectangle is calculated to be the point of intersection of its diagonals.
     *
     * @return the center of this rectangle.
     */
    @Override
    public Point center() {
        List<TwoDPoint> points = getPosition();
        double m1 = calculateSlope(points.get(0), points.get(2));
        double b1 = calculateYIntercept(points.get(0), m1);
        double m2 = calculateSlope(points.get(1), points.get(3));
        double b2 = calculateYIntercept(points.get(1), m2);
        double xIntersection = (b2 - b1) / (m1 - m2);
        double yIntersection = m1 * xIntersection + b1;
        return new TwoDPoint(xIntersection, yIntersection);
    }

    private double calculateSlope(Point pointOne, Point pointTwo) {
        double[] pointOneCoords = pointOne.coordinates();
        double[] pointTwoCoords = pointTwo.coordinates();
        return (pointTwoCoords[1] - pointOneCoords[1]) / (pointTwoCoords[0] - pointOneCoords[0]);
    }

    private double calculateYIntercept(Point point, double slope) {
        double[] pointCoords = point.coordinates();
        return (pointCoords[1] - (pointCoords[0] * slope));
    }

    @Override
    public boolean isMember(List<? extends Point> vertices) {
        if (!super.isMember(vertices)) return false;
        Quadrilateral quadrilateral = new Quadrilateral(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        quadrilateral.setPosition(vertices);
        double[] sideLengths = quadrilateral.getSideLengths();
        if ((sideLengths[0] == sideLengths[2]) && (sideLengths[1] == sideLengths[3])) {
            return (super.calculateLength((TwoDPoint) vertices.get(0), (TwoDPoint) vertices.get(2))) == (super.calculateLength((TwoDPoint) vertices.get(1), (TwoDPoint) vertices.get(3)));
        } else {
            return false;
        }
    }

    /*
    @Override
    public boolean isMember(List<? extends Point> vertices) {
        if (!super.isMember(vertices)) return false;
        return (super.calculateLength((TwoDPoint) vertices.get(0), (TwoDPoint) vertices.get(1))) == (super.calculateLength((TwoDPoint) vertices.get(1), (TwoDPoint) vertices.get(3)));
    }
    */

    @Override
    public double area() {
        double length, width;
        double[] sideLengths = getSideLengths();
        length = sideLengths[0];
        width = sideLengths[1];
        return length * width;
    }
}
